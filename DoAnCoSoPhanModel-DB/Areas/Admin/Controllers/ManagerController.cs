﻿using DoAnCoSoPhanModel_DB.Models;
using DoAnCoSoPhanModel_DB.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DoAnCoSoPhanModel_DB.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class ManagerController : Controller
    {
        private readonly ISubjectRepository _subjectRepository;
        private readonly IDocumentsRepository _documentsRepository;
        private readonly ICommentsRepository _commentsRepository;
        private readonly IFeedBacksRepository _feedBacksRepository;
        private readonly ITeacherRepository _teacherRepository;
        private readonly INewsRepository _newsRepository;
        private readonly IPostRepository _postRepository;
        private readonly IStudentRepository _studentRepository;
        public ManagerController(ISubjectRepository subjectRepository, IDocumentsRepository documentsRepository, ICommentsRepository commentsRepository, IFeedBacksRepository feedBacksRepository, ITeacherRepository teacherRepository, INewsRepository newsRepository, IPostRepository postRepository, IStudentRepository studentRepository)
        {
            _subjectRepository = subjectRepository;
            _documentsRepository = documentsRepository;
            _commentsRepository = commentsRepository;
            _feedBacksRepository = feedBacksRepository;
            _teacherRepository = teacherRepository;
            _newsRepository = newsRepository;
            _postRepository = postRepository;
            _studentRepository = studentRepository;
        }
        public IActionResult Index()
        {
            return View();
        }

        //===============================cac mon hoc================================================
        public async Task<IActionResult> IndexCacMonHoc()
        {
            var cacmonhoc = await _subjectRepository.GetAllAsync();
            return View(cacmonhoc);
        }

        //Hiển thị form thêm môn học mới
        public async Task<IActionResult> CreateSubject()
        {
            var subjects = await _subjectRepository.GetAllAsync();

            return View();
        }


        // Xử lý thêm sản phẩm mới
        [HttpPost]
        public async Task<IActionResult> CreateSubject(Subject subject, IFormFile ImageUrl)
        {
            if (ModelState.IsValid)
            {

                if (ImageUrl != null)
                {
                    // Lưu hình ảnh đại diện
                    subject.ImageUrl = await SaveImage(ImageUrl);
                }
                await _subjectRepository.AddAsync(subject);
                return RedirectToAction(nameof(IndexCacMonHoc));
            }
            // Nếu ModelState không hợp lệ, hiển thị form với dữ liệu đã nhập
            var subjects = await _subjectRepository.GetAllAsync();

            return View(subject);
        }

        private async Task<string> SaveImage(IFormFile ImageUrl)
        {
            var savePath = Path.Combine("wwwroot/Subject/images", ImageUrl.FileName); // Thay đổi đường dẫn theo cấu hình của bạn
            using (var fileStream = new FileStream(savePath, FileMode.Create))
            {
                await ImageUrl.CopyToAsync(fileStream);
            }
            return "/Subject/images/" + ImageUrl.FileName; // Trả về đường dẫn tương đối
        }

        public async Task<IActionResult> DetailsSubject(Guid id)
        {
            var subject = await _subjectRepository.GetByIdAsync(id);
            if (subject == null)
            {
                return NotFound();
            }
            return View(subject);
        }

        public async Task<IActionResult> EditSubject(Guid id)
        {
            var subject = await _subjectRepository.GetByIdAsync(id);
            if (subject == null)
            {
                return NotFound();
            }
            return View(subject);
        }
        // Process the product update
        [HttpPost]
        public async Task<IActionResult> EditSubject(Guid id, Subject subject, IFormFile ImageUrl)
        {
            ModelState.Remove("ImageUrl");
            var existingSubject = await _subjectRepository.GetByIdAsync(id);
            if (id != subject.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                if (ImageUrl != null)
                {
                    // Lưu hình ảnh đại diện
                    subject.ImageUrl = await SaveImage(ImageUrl);
                }
                else
                {
                    subject.ImageUrl = existingSubject.ImageUrl;
                }
                existingSubject.Name = subject.Name;
                existingSubject.Description = subject.Description;
                existingSubject.ImageUrl = subject.ImageUrl;
                existingSubject.Documents = subject.Documents;
                await _subjectRepository.UpdateAsync(existingSubject);
                return RedirectToAction(nameof(IndexCacMonHoc));
            }

            return View(subject);
        }


        public async Task<IActionResult> DeleteSubject(Guid id)
        {
            var subject = await _subjectRepository.GetByIdAsync(id);
            if (subject == null)
            {
                return NotFound();
            }
            return View(subject);
        }
        // Xử lý xóa sản phẩm
        [HttpPost, ActionName("DeleteSubject")]
        public async Task<IActionResult> DeleteSubject1(Guid id)
        {
            await _subjectRepository.DeleteAsync(id);
            return RedirectToAction(nameof(IndexCacMonHoc));
        }

        //===============================document================================================

        public async Task<IActionResult> IndexDocument()
        {
            var d = await _documentsRepository.GetAllAsync();
            ViewBag.NumberOfFB = d.Count();
            return View(d);
        }

        public async Task<IActionResult> CreateDocument()
        {
            var d = await _documentsRepository.GetAllAsync();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateDocument(Documents d, IFormFile ImageUrl)
        {
            if (ModelState.IsValid)
            {
                if (ImageUrl != null)
                {
                    // Lưu hình ảnh đại diện
                    d.ImageUrl = await SaveImage(ImageUrl);
                }
                await _documentsRepository.AddAsync(d);
                return RedirectToAction(nameof(IndexDocument));
            }
            // Nếu ModelState không hợp lệ, hiển thị form với dữ liệu đã nhập

            return View(d);
        }

        public async Task<IActionResult> DetailsDocument(Guid id)
        {
            var d = await _documentsRepository.GetByIdAsync(id);
            if (d == null)
            {
                return NotFound();
            }
            return View(d);
        }

        public async Task<IActionResult> DeleteDocument(Guid id)
        {
            var d = await _documentsRepository.GetByIdAsync(id);
            if (d == null)
            {
                return NotFound();
            }
            return View(d);
        }
        // Xử lý xóa sản phẩm
        [HttpPost, ActionName("DeleteDocument")]
        public async Task<IActionResult> DeleteDocument1(Guid id)
        {
            await _documentsRepository.DeleteAsync(id);
            return RedirectToAction(nameof(IndexDocument));
        }

        public async Task<IActionResult> EditDocument(Guid id)
        {
            var d = await _documentsRepository.GetByIdAsync(id);
            if (d == null)
            {
                return NotFound();
            }
            return View(d);
        }
        // Process the product update
        [HttpPost]
        public async Task<IActionResult> EditDocument(Guid id, Documents d, IFormFile ImageUrl)
        {
            ModelState.Remove("ImageUrl");
            var existingDocument = await _documentsRepository.GetByIdAsync(id);
            if (id != d.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                if (ImageUrl != null)
                {
                    // Lưu hình ảnh đại diện
                    d.ImageUrl = await SaveImage(ImageUrl);
                }
                else
                {
                    d.ImageUrl = existingDocument.ImageUrl;
                }
                existingDocument.title = d.title;
                existingDocument.Content = d.Content;
                existingDocument.ImageUrl = d.ImageUrl;
                existingDocument.files = d.files;
                existingDocument.videos = d.videos;
                await _documentsRepository.UpdateAsync(existingDocument);
                return RedirectToAction(nameof(IndexDocument));
            }

            return View(d);
        }

        //===============================Feedback================================================

        public async Task<IActionResult> CreateFeedback()
        {
            var feedbacks = await _feedBacksRepository.GetAllAsync();

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateFeedback(Feedbacks feedbacks)
        {
            if (ModelState.IsValid)
            {
                await _feedBacksRepository.AddAsync(feedbacks);
                return RedirectToAction(nameof(GetAllFeedbacks));
            }
            // Nếu ModelState không hợp lệ, hiển thị form với dữ liệu đã nhập

            return View(feedbacks);
        }


        public async Task<IActionResult> GetAllFeedbacks()
        {
            var feedbacks = await _feedBacksRepository.GetAllAsync();
            ViewBag.NumberOfFB = feedbacks.Count();
            return View(feedbacks);
        }

        public async Task<IActionResult> EditFeedback(Guid id)
        {
            var feedbacks = await _feedBacksRepository.GetByIdAsync(id);
            if (feedbacks == null)
            {
                return NotFound();
            }
            return View(feedbacks);
        }

        [HttpPost]
        public async Task<IActionResult> EditFeedback(Guid id, Feedbacks fb)
        {
            var existingFb = await _feedBacksRepository.GetByIdAsync(id);
            if (id != fb.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                existingFb.title = fb.title;
                existingFb.content = fb.content;

                await _feedBacksRepository.UpdateAsync(existingFb);
                return RedirectToAction(nameof(GetAllFeedbacks));
            }

            return View(fb);
        }

        public async Task<IActionResult> DeleteFeedback(Guid id)
        {
            var feedbacks = await _feedBacksRepository.GetByIdAsync(id);
            if (feedbacks == null)
            {
                return NotFound();
            }
            return View(feedbacks);
        }
        // Xử lý xóa sản phẩm
        [HttpPost, ActionName("DeleteFeedback")]
        public async Task<IActionResult> DeleteFeedback1(Guid id)
        {
            await _feedBacksRepository.DeleteAsync(id);
            return RedirectToAction(nameof(GetAllFeedbacks));
        }

        //===============================Giao vien================================================

        public async Task<IActionResult> GetAllGiaoVien()
        {
            var gv = await _teacherRepository.GetAllAsync();
            ViewBag.NumberOfFB = gv.Count();
            return View(gv);
        }


        public async Task<IActionResult> CreateGiaoVien()
        {
            var gv = await _teacherRepository.GetAllAsync();
            return View();
        }


        [HttpPost]
        public async Task<IActionResult> CreateGiaoVien(Teacher gv, IFormFile ImageUrl)
        {
            if (ModelState.IsValid)
            {
                if (ImageUrl != null)
                {
                    // Lưu hình ảnh đại diện
                    gv.ImageUrl = await SaveImage(ImageUrl);
                }

                await _teacherRepository.AddAsync(gv);
                return RedirectToAction(nameof(GetAllGiaoVien));
            }

            return View(gv);
        }


        public async Task<IActionResult> DeleteGiaoVien(Guid id)
        {
            var gv = await _teacherRepository.GetByIdAsync(id);
            if (gv == null)
            {
                return NotFound();
            }
            return View(gv);
        }
        // Xử lý xóa sản phẩm
        [HttpPost, ActionName("DeleteGiaoVien")]
        public async Task<IActionResult> DeleteGiaoVien1(Guid id)
        {
            await _teacherRepository.DeleteAsync(id);
            return RedirectToAction(nameof(GetAllGiaoVien));
        }


        public async Task<IActionResult> EditGiaoVien(Guid id)
        {
            var gv = await _teacherRepository.GetByIdAsync(id);
            if (gv == null)
            {
                return NotFound();
            }
            return View(gv);
        }

        [HttpPost]
        public async Task<IActionResult> EditGiaoVien(Guid id, Teacher gv, IFormFile ImageUrl)
        {
            ModelState.Remove("ImageUrl");
            var existingGV = await _teacherRepository.GetByIdAsync(id);
            if (id != gv.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                if (ImageUrl != null)
                {
                    // Lưu hình ảnh đại diện mới nếu có
                    gv.ImageUrl = await SaveImage(ImageUrl);
                }
                else
                {
                    // Giữ nguyên hình ảnh đại diện cũ nếu không có hình ảnh mới
                    gv.ImageUrl = existingGV.ImageUrl;
                }

                // Cập nhật thông tin của giáo viên từ form vào giáo viên hiện tại
                existingGV.Email = gv.Email;
                existingGV.FullName = gv.FullName;
                existingGV.age = gv.age;
                existingGV.Gender = gv.Gender;
                existingGV.MonDay = gv.MonDay;
                existingGV.Address = gv.Address;
                existingGV.PhoneNumber = gv.PhoneNumber;
                existingGV.WorkExperience = gv.WorkExperience;
                existingGV.ImageUrl = gv.ImageUrl; // Gán hình ảnh mới hoặc giữ nguyên hình ảnh cũ

                // Lưu thông tin giáo viên đã được cập nhật
                await _teacherRepository.UpdateAsync(existingGV);

                return RedirectToAction(nameof(GetAllGiaoVien));
            }

            return View(gv);
        }
        //===============================COMMENT================================================
        public async Task<ActionResult> IndexCmt()
        {
            var comment = await _commentsRepository.GetAllAsync();
            return View(comment);
        }

        // GET: CommentsController/Details/5
        public async Task<ActionResult> DetailsCmt(Guid id)
        {
            var comment = await _commentsRepository.GetByIdAsync(id);
            return View(comment);
        }

        // GET: CommentsController/Create
        public async Task<ActionResult> CreateCmt()
        {
            var comment = await _commentsRepository.GetAllAsync();
            return View();
        }

        // POST: CommentsController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateCmt(Comments comments)
        {
            if (ModelState.IsValid)
            {
                comments.time_create = DateTime.Now;
                await _commentsRepository.AddAsync(comments);
                return RedirectToAction(nameof(IndexCmt));

            }
            return View(comments);
        }


        // GET: CommentsController/Edit/5
        public async Task<ActionResult> EditCmt(Guid id)
        {
            var comment = await _commentsRepository.GetByIdAsync(id);
            if (comment == null)
            {
                return NotFound();
            }

            return View(comment);
        }

        // POST: CommentsController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditCmt(Guid id, Comments editComments)
        {
            try
            {
                if (id != editComments.Id)
                {
                    return NotFound();
                }

                if (ModelState.IsValid)
                {
                    var existingComment = await _commentsRepository.GetByIdAsync(id);
                    if (existingComment == null)
                    {
                        return NotFound();
                    }

                    // Update other properties
                    existingComment.title = editComments.title;
                    existingComment.contents = editComments.contents;
                    existingComment.time_update = DateTime.Now; // Update the time of update

                    await _commentsRepository.UpdateAsync(existingComment);

                    return RedirectToAction(nameof(IndexCmt));
                }
                return RedirectToAction(nameof(IndexCmt));
            }
            catch
            {
                return View(editComments);
            }
        }

        // GET: CommentsController/Delete/5
        public async Task<ActionResult> DeleteCmt(Guid id)
        {
            var comment = await _commentsRepository.GetByIdAsync(id);
            if (comment == null)
            {
                return NotFound();
            }
            return View(comment);
        }

        // POST: CommentsController/Delete/5
        [HttpPost, ActionName("DeleteCmt")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteCmt(Guid id, IFormCollection collection)
        {
            try
            {
                await _commentsRepository.DeleteAsync(id);
                return RedirectToAction(nameof(IndexCmt));
            }
            catch
            {
                return View();
            }
        }



        //===============================News================================================
        public async Task<IActionResult> IndexNews()
        {
            var news = await _newsRepository.GetAllAsync();
            ViewBag.NumberOfFB = news.Count();
            return View(news);
        }

        public async Task<IActionResult> CreateNews()
        {
            var news = await _newsRepository.GetAllAsync();
            return View();
        }


        [HttpPost]
        public async Task<IActionResult> CreateNews(News news, IFormFile ImageUrl)
        {
            if (ModelState.IsValid)
            {
                if (ImageUrl != null)
                {
                    // Lưu hình ảnh đại diện
                    news.ImageUrl = await SaveImage(ImageUrl);
                }
                news.time_create = DateTime.Now;
                await _newsRepository.AddAsync(news);
                return RedirectToAction(nameof(IndexNews));
            }

            return View(news);
        }

        public async Task<IActionResult> DetailsNews(Guid id)
        {
            var news = await _newsRepository.GetByIdAsync(id);
            if (news == null)
            {
                return NotFound();
            }
            return View(news);
        }


        public async Task<IActionResult> EditNews(Guid id)
        {
            var news = await _newsRepository.GetByIdAsync(id);
            if (news == null)
            {
                return NotFound();
            }
            return View(news);
        }
        // Process the product update
        [HttpPost]
        public async Task<IActionResult> Edit(Guid id, News news, IFormFile ImageUrl)
        {
            ModelState.Remove("ImageUrl");
            var existingNews = await _newsRepository.GetByIdAsync(id);
            if (id != news.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                if (ImageUrl != null)
                {
                    // Lưu hình ảnh đại diện
                    news.ImageUrl = await SaveImage(ImageUrl);
                }
                else
                {
                    news.ImageUrl = existingNews.ImageUrl;
                }
                existingNews.Title = news.Title;
                existingNews.Content = news.Content;

                existingNews.time_update = DateTime.Now;

                existingNews.ImageUrl = news.ImageUrl;
                existingNews.time_create = existingNews.time_create;
                await _newsRepository.UpdateAsync(existingNews);
                return RedirectToAction(nameof(IndexNews));
            }

            return View(news);
        }

        public async Task<IActionResult> DeleteNews(Guid id)
        {
            var news = await _newsRepository.GetByIdAsync(id);
            if (news == null)
            {
                return NotFound();
            }
            return View(news);
        }
        // Xử lý xóa sản phẩm
        [HttpPost, ActionName("DeleteNews")]
        public async Task<IActionResult> DeleteNews1(Guid id)
        {
            await _newsRepository.DeleteAsync(id);
            return RedirectToAction(nameof(IndexNews));
        }

        //===============================POST================================================

        public async Task<IActionResult> IndexList()
        {
            var posts = await _postRepository.GetAllAsync();
            return View(posts);
        }

        public async Task<IActionResult> AddPost()
        {
            var post = await _postRepository.GetAllAsync();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddPost(Post post, IFormFile ImageUrl)
        {
            if (ModelState.IsValid)
            {
                if (ImageUrl != null)
                {
                    // Lưu hình ảnh đại diện
                    post.ImageUrl = await SaveImage(ImageUrl);
                }
                post.time_create = DateTime.Now;
                await _postRepository.AddAsync(post);
                return RedirectToAction(nameof(IndexList));
            }
            return View(post);
        }

        public async Task<IActionResult> DisplayPost(Guid id)
        {
            var post = await _postRepository.GetByIdAsync(id);
            if (post == null)
            {
                return NotFound();
            }
            return View(post);
        }

        public async Task<IActionResult> DeletePost(Guid id)
        {
            var post = await _postRepository.GetByIdAsync(id);
            if (post == null)
            {
                return NotFound();
            }
            return View(post);
        }

        [HttpPost, ActionName("DeletePost")]
        public async Task<IActionResult> DeletePost1(Guid id)
        {
            await _postRepository.DeleteAsync(id);
            return RedirectToAction(nameof(IndexList));
        }

        public async Task<IActionResult> UpdatePost(Guid id)
        {
            var post = await _postRepository.GetByIdAsync(id);
            if (post == null)
            {
                return NotFound();
            }

            return View(post);
        }

        [HttpPost]
        public async Task<IActionResult> UpdatePost(Guid id, Post updatedPost, IFormFile ImageUrl)
        {
            ModelState.Remove("ImageUrl");
            var existingPost = await _postRepository.GetByIdAsync(id);
            if (id != updatedPost.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                if (ImageUrl != null)
                {
                    // Lưu hình ảnh đại diện
                    updatedPost.ImageUrl = await SaveImage(ImageUrl);
                }
                else
                {
                    updatedPost.ImageUrl = existingPost.ImageUrl;
                }

                // Update other properties

                existingPost.title = updatedPost.title;
                existingPost.Content = updatedPost.Content;

                existingPost.time_update = DateTime.Now;


                existingPost.ImageUrl = updatedPost.ImageUrl;
                existingPost.time_create = existingPost.time_create;

                await _postRepository.UpdateAsync(existingPost);

                return RedirectToAction(nameof(IndexList));
            }

            return View(updatedPost);
        }


        //===============================Hoc Sinh================================================

        public async Task<IActionResult> IndexHocSinh()
        {
            var hs = await _studentRepository.GetAllAsync();
            return View(hs);
        }


        public async Task<IActionResult> CreateHocSinh()
        {
            var hs = await _studentRepository.GetAllAsync();
            return View();
        }


        [HttpPost]
        public async Task<IActionResult> CreateHocSinh(Student hs, IFormFile ImageUrl)
        {
            if (ModelState.IsValid)
            {
                if (ImageUrl != null)
                {
                    // Lưu hình ảnh đại diện
                    hs.ImageUrl = await SaveImage(ImageUrl);
                }
                await _studentRepository.AddAsync(hs);
                return RedirectToAction(nameof(IndexHocSinh));
            }

            return View(hs);
        }

        //===============================================
        public async Task<IActionResult> EditHocSinh(Guid id)
        {
            var hs = await _studentRepository.GetByIdAsync(id);
            if (hs == null)
            {
                return NotFound();
            }
            return View(hs);
        }

        [HttpPost]
        public async Task<IActionResult> EditHocSinh(Guid id, Student hs, IFormFile ImageUrl)
        {
            ModelState.Remove("ImageUrl");
            var existingHS = await _studentRepository.GetByIdAsync(id);
            if (id != hs.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                if (ImageUrl != null)
                {
                    // Lưu hình ảnh đại diện mới nếu có
                    hs.ImageUrl = await SaveImage(ImageUrl);
                }
                else
                {
                    // Giữ nguyên hình ảnh đại diện cũ nếu không có hình ảnh mới
                    hs.ImageUrl = existingHS.ImageUrl;
                }

                // Cập nhật thông tin của giáo viên từ form vào giáo viên hiện tại
                existingHS.Email = hs.Email;
                existingHS.FullName = hs.FullName;
                existingHS.Grade = hs.Grade;
                existingHS.Gender = hs.Gender;
                existingHS.Address = hs.Address;
                existingHS.PhoneNumber = hs.PhoneNumber;
                existingHS.ImageUrl = hs.ImageUrl; // Gán hình ảnh mới hoặc giữ nguyên hình ảnh cũ

                // Lưu thông tin giáo viên đã được cập nhật
                await _studentRepository.UpdateAsync(existingHS);

                return RedirectToAction(nameof(IndexHocSinh));
            }

            return View(hs);
        }

        public async Task<IActionResult> DetailsHocSinh(Guid id)
        {
            var hs = await _studentRepository.GetByIdAsync(id);
            if (hs == null)
            {
                return NotFound();
            }
            return View(hs);
        }


        public async Task<IActionResult> DeleteHocSinh(Guid id)
        {
            var hs = await _studentRepository.GetByIdAsync(id);
            if (hs == null)
            {
                return NotFound();
            }
            return View(hs);
        }
        // Xử lý xóa sản phẩm
        [HttpPost, ActionName("DeleteHocSinh")]
        public async Task<IActionResult> DeleteHocSinh1(Guid id)
        {
            await _studentRepository.DeleteAsync(id);
            return RedirectToAction(nameof(IndexHocSinh));
        }
    }
}
