﻿namespace DoAnCoSoPhanModel_DB.Models
{
    public class Comments
    {
        public Guid Id { get; set; }
        public string title { get; set; }

        public string contents { get; set; }

        public DateTime time_create { get; set; }

        public DateTime time_update { get; set; }

        public Post? post { get; set; }

        public virtual ApplicationUser? applicationUser { get; set; }
    }
}
