﻿using System.ComponentModel.DataAnnotations.Schema;

using System.ComponentModel.DataAnnotations;

namespace DoAnCoSoPhanModel_DB.Models
{
    public class Feedbacks
    {
        public Guid Id { get; set; }
        [Required]
        public string title { get; set; }
        [Required]
        public string content { get; set; }
        public Student? student { get; set; }
    }
}
