﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Numerics;

namespace DoAnCoSoPhanModel_DB.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string FullName { get; set; }
        
        public List<Subject> subjects { get; set; }

        public List<News> news { get; set; }
        public List<Post> posts { get; set; }

        public List<Comments> comments { get; set; }
        
        public List<Student> students { get; set; }
        public List<Teacher> teachers { get; set; }
    }
}
