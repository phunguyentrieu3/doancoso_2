﻿using System.ComponentModel.DataAnnotations;

namespace DoAnCoSoPhanModel_DB.Models
{
    public class Student
    {
        public Guid Id { get; set; }

        // Thông tin cá nhân
        [Required(ErrorMessage = "Vui lòng nhập họ và tên.")]
        public string FullName { get; set; }

        public string Address { get; set; }

        public string PhoneNumber { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        public string Gender { get; set; }

        public string? ImageUrl { get; set; }

        public string Grade { get; set; }

        public List<Feedbacks>? feedbacks { get; set; }

        public virtual ApplicationUser? ApplicationUser { get; set; }
    }
}
