﻿using System.ComponentModel.DataAnnotations;

namespace DoAnCoSoPhanModel_DB.Models
{
    public class Subject
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập tên môn học.")]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }

        public string? ImageUrl { get; set; }

        public List<Documents>? Documents { get; set; }
        public virtual ApplicationUser? ApplicationUser { get; set; }

    }
}
