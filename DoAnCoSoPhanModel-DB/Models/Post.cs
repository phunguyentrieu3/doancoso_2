﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DoAnCoSoPhanModel_DB.Models
{
    public class Post
    {
        public Guid Id { get; set; }
        [Required]
        public string title { get; set; }
        [Required]
        public string Content { get; set; }
        public string? ImageUrl { get; set; }
        public DateTime time_create { get; set; }
        public DateTime time_update { get; set; }
        public virtual ApplicationUser? applicationUser { get; set; }
    }
}
