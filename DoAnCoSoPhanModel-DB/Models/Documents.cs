﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DoAnCoSoPhanModel_DB.Models
{
    public class Documents
    {
        public Guid Id { get; set; }
        [Required]
        public string title { get; set; }
        [Required]
        public string Content { get; set; }

        public string? ImageUrl { get; set; }

        public String? files { get; set; }

        public String? videos { get; set; }

        public Subject? subject { get; set; }

        public Teacher? teacher { get; set; }
    }
}
