﻿namespace DoAnCoSoPhanModel_DB.Models
{
    public class CategoryGrade
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Subject? subject { get; set; }
    }
}
