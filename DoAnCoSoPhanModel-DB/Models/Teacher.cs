﻿using System.ComponentModel.DataAnnotations;

namespace DoAnCoSoPhanModel_DB.Models
{
    public class Teacher
    {
        public Guid Id { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập họ và tên.")]
        public string FullName { get; set; }

        public int age { get; set; }

        public string Gender { get; set; }

        public string MonDay { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        public string PhoneNumber { get; set; }
        [Required]
        public string WorkExperience { get; set; } // Kinh nghiệm làm việc
        public string? ImageUrl { get; set; }
        public virtual ApplicationUser? ApplicationUser { get; set; }

        public List<Documents>? documents { get; set; }
    }
}
