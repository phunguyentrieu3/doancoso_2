﻿using DoAnCoSoPhanModel_DB.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Text.RegularExpressions;

namespace DoAnCoSoPhanModel_DB.DataAccess
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser> { 
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }
        public DbSet<Documents> documents { get; set; }
        public DbSet<Feedbacks> feedbacks { get; set; }
        public DbSet<News> news { get; set; }
        public DbSet<Post> posts { get; set; }
        public DbSet<Student> students_detail { get; set; }
        public DbSet<Subject> subjects { get; set; }
        public DbSet<Teacher> teachers_detail { get; set; }
        public DbSet<Comments> comments {  get; set; }
        public DbSet<CategoryGrade> categoryGrades { get; set;}

    }
}
