using DoAnCoSoPhanModel_DB.DataAccess;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using DoAnCoSoPhanModel_DB.Models;
using DoAnCoSoPhanModel_DB.Repositories;
using A2_SinhToDau_QuanLyBenhVien.Repositories;
using Microsoft.AspNetCore.Identity.UI.Services;

var builder = WebApplication.CreateBuilder(args);


builder.Services.AddDbContext<ApplicationDbContext>(options =>
options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));


builder.Services.AddIdentity<ApplicationUser, IdentityRole>(options =>
{
    //cau hinh ...
})
    .AddDefaultUI()
    .AddEntityFrameworkStores<ApplicationDbContext>()
    .AddDefaultTokenProviders();
builder.Services.AddRazorPages();

// Add services to the container.
builder.Services.AddControllersWithViews();
builder.Services.AddScoped<IPostRepository, EFPostRepository>();
builder.Services.AddScoped<INewsRepository, EFNewsRepository>();
builder.Services.AddScoped<IFeedBacksRepository, EFFeedBacksRepository>();
builder.Services.AddScoped<ISubjectRepository, EFSubjectRepository>();
builder.Services.AddScoped<IDocumentsRepository, EFDocumentsRepository>();
builder.Services.AddScoped<ICategoryGradeRepository, EFCategoryGradeRepository>();
builder.Services.AddScoped<ICommentsRepository, EFCommentsRepository>();
builder.Services.AddScoped<ITeacherRepository, EFTeacherRepository>();
builder.Services.AddScoped<IStudentRepository, EFStudentRepository>();
builder.Services.AddTransient<IEmailSender, EmailSender>();
var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
}
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapRazorPages();

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllerRoute(
      name: "areas",
      pattern: "{area:exists}/{controller=Manager}/{action=Index}/{id?}"
    );
});

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
