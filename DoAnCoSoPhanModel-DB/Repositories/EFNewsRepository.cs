﻿using DoAnCoSoPhanModel_DB.DataAccess;
using DoAnCoSoPhanModel_DB.Models;
using Microsoft.EntityFrameworkCore;

namespace DoAnCoSoPhanModel_DB.Repositories
{
    public class EFNewsRepository : INewsRepository
    {
        private readonly ApplicationDbContext _context;
        public EFNewsRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task AddAsync(News news)
        {
            _context.news.Add(news);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            var news1 = await _context.news.FindAsync(id);
            _context.news.Remove(news1);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<News>> GetAllAsync()
        {
            return await _context.news.ToListAsync();
        }

        public  async Task<News> GetByIdAsync(Guid id)
        {
            return await _context.news.FindAsync(id);
        }

        public async Task UpdateAsync(News news)
        {
            _context.news.Update(news);
            await _context.SaveChangesAsync();
        }
        public List<News> GetAll()
        {
            return _context.news.ToList();
        }
    }
}
