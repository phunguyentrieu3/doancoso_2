﻿using DoAnCoSoPhanModel_DB.DataAccess;
using DoAnCoSoPhanModel_DB.Models;
using Microsoft.EntityFrameworkCore;

namespace DoAnCoSoPhanModel_DB.Repositories
{
    public class EFStudentRepository : IStudentRepository
    {
        private readonly ApplicationDbContext _context;
        public EFStudentRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task AddAsync(Student student)
        {
            _context.students_detail.Add(student);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            var st = await _context.students_detail.FindAsync(id);
            _context.students_detail.Remove(st);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Student>> GetAllAsync()
        {
            return await _context.students_detail.ToListAsync();
        }

        public async Task<Student> GetByIdAsync(Guid id)
        {
            return await _context.students_detail.FindAsync(id);
        }

        public async Task UpdateAsync(Student student)
        {
            _context.students_detail.Update(student);
            await _context.SaveChangesAsync();
        }
        public List<Student> GetAll()
        {
            return _context.students_detail.ToList();
        }
    }
}
