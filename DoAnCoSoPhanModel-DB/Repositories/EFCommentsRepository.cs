﻿using DoAnCoSoPhanModel_DB.DataAccess;
using DoAnCoSoPhanModel_DB.Models;
using Microsoft.EntityFrameworkCore;

namespace DoAnCoSoPhanModel_DB.Repositories
{
    public class EFCommentsRepository : ICommentsRepository
    {
        private readonly ApplicationDbContext _context;

        public EFCommentsRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Comments>> GetAllAsync()
        {
            return await _context.comments.ToListAsync();
        }

        public async Task<Comments> GetByIdAsync(Guid id)
        {
            return await _context.comments.FindAsync(id);
        }

        public async Task AddAsync(Comments comments)
        {
            _context.comments.Add(comments);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(Comments comments)
        {
            _context.comments.Update(comments);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            var comments = await _context.comments.FindAsync(id);
            _context.comments.Remove(comments);
            await _context.SaveChangesAsync();
        }
    }
}
