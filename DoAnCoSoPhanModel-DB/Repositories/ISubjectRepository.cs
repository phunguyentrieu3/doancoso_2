﻿using DoAnCoSoPhanModel_DB.Models;

namespace DoAnCoSoPhanModel_DB.Repositories
{
    public interface ISubjectRepository
    {
        Task<IEnumerable<Subject>> GetAllAsync();
        Task<Subject> GetByIdAsync(Guid id);
        Task AddAsync(Subject subject);
        Task UpdateAsync(Subject subject);
        Task DeleteAsync(Guid id);
        List<Subject> GetAll();
    }
}
