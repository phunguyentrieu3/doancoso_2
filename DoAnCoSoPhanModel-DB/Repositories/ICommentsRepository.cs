﻿using DoAnCoSoPhanModel_DB.Models;

namespace DoAnCoSoPhanModel_DB.Repositories
{
    public interface ICommentsRepository
    {
        Task<IEnumerable<Comments>> GetAllAsync();
        Task<Comments> GetByIdAsync(Guid id);
        Task AddAsync(Comments categoryGrade);
        Task UpdateAsync(Comments categoryGrade);
        Task DeleteAsync(Guid id);
    }
}
