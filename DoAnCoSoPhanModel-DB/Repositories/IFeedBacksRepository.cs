﻿using DoAnCoSoPhanModel_DB.Models;

namespace DoAnCoSoPhanModel_DB.Repositories
{
    public interface IFeedBacksRepository
    {
        Task<IEnumerable<Feedbacks>> GetAllAsync();
        Task<Feedbacks> GetByIdAsync(Guid id);
        Task AddAsync(Feedbacks feedbacks);
        Task UpdateAsync(Feedbacks feedbacks);
        Task DeleteAsync(Guid id);
        List<Feedbacks> GetAll();
    }
}
