﻿using DoAnCoSoPhanModel_DB.DataAccess;
using DoAnCoSoPhanModel_DB.Models;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.EntityFrameworkCore;

namespace DoAnCoSoPhanModel_DB.Repositories
{
    public class EFFeedBacksRepository : IFeedBacksRepository
    {
        private readonly ApplicationDbContext _context;
        public  EFFeedBacksRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task AddAsync(Feedbacks feedbacks)
        {
            _context.feedbacks.Add(feedbacks);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            var feedback1 = await _context.feedbacks.FindAsync(id);
            _context.feedbacks.Remove(feedback1);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Feedbacks>> GetAllAsync()
        {
            return await _context.feedbacks.ToListAsync();
        }

        public async Task<Feedbacks> GetByIdAsync(Guid id)
        {
            return await _context.feedbacks.FindAsync(id);
        }

        public async Task UpdateAsync(Feedbacks feedbacks)
        {
            _context.feedbacks.Update(feedbacks);
            await _context.SaveChangesAsync();
        }
        public List<Feedbacks> GetAll()
        {
            return _context.feedbacks.ToList();
        }
    }
}
