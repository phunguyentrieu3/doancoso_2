﻿using DoAnCoSoPhanModel_DB.DataAccess;
using DoAnCoSoPhanModel_DB.Models;
using Microsoft.EntityFrameworkCore;
using System.Numerics;

namespace DoAnCoSoPhanModel_DB.Repositories
{
    public class EFCategoryGradeRepository : ICategoryGradeRepository
    {
        private readonly ApplicationDbContext _context;

        public EFCategoryGradeRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<CategoryGrade>> GetAllAsync()
        {
            return await _context.categoryGrades.ToListAsync();
        }

        public async Task<CategoryGrade> GetByIdAsync(Guid id)
        {
            return await _context.categoryGrades.FindAsync(id);
        }

        public async Task AddAsync(CategoryGrade categoryGrades)
        {
            _context.categoryGrades.Add(categoryGrades);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(CategoryGrade categoryGrades)
        {
            _context.categoryGrades.Update(categoryGrades);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            var categoryGrades = await _context.categoryGrades.FindAsync(id);
            _context.categoryGrades.Remove(categoryGrades);
            await _context.SaveChangesAsync();
        }
    }
}
