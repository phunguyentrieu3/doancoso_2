﻿using DoAnCoSoPhanModel_DB.Models;

namespace DoAnCoSoPhanModel_DB.Repositories
{
    public interface INewsRepository
    {
        Task<IEnumerable<News>> GetAllAsync();
        Task<News> GetByIdAsync(Guid id);
        Task AddAsync(News news);
        Task UpdateAsync(News news);
        Task DeleteAsync(Guid id);
        List<News> GetAll();
    }
}
