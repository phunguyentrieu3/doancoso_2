﻿using DoAnCoSoPhanModel_DB.DataAccess;
using DoAnCoSoPhanModel_DB.Models;
using Microsoft.EntityFrameworkCore;

namespace DoAnCoSoPhanModel_DB.Repositories
{
    public class EFSubjectRepository : ISubjectRepository
    {
        private readonly ApplicationDbContext _context;
        public EFSubjectRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task AddAsync(Subject subject)
        {
            _context.subjects.Add(subject);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            var subject1 = await _context.subjects.FindAsync(id);
            _context.subjects.Remove(subject1);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Subject>> GetAllAsync()
        {
            return await _context.subjects.ToListAsync();
        }

        public async Task<Subject> GetByIdAsync(Guid id)
        {
            return await _context.subjects.FindAsync(id);
        }

        public async Task UpdateAsync(Subject subject)
        {
            _context.subjects.Update(subject);
            await _context.SaveChangesAsync();
        }
        public List<Subject> GetAll()
        {
            return _context.subjects.ToList();
        }
    }
}
