﻿using DoAnCoSoPhanModel_DB.Models;

namespace DoAnCoSoPhanModel_DB.Repositories
{
    public interface IDocumentsRepository
    {
        Task<IEnumerable<Documents>> GetAllAsync();
        Task<Documents> GetByIdAsync(Guid id);
        Task AddAsync(Documents documents);
        Task UpdateAsync(Documents documents);
        Task DeleteAsync(Guid id);
        List<Documents> GetAll();
    }
}
