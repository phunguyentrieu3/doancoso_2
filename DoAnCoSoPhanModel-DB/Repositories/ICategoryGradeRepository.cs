﻿using DoAnCoSoPhanModel_DB.Models;

namespace DoAnCoSoPhanModel_DB.Repositories
{
    public interface ICategoryGradeRepository
    {
        Task<IEnumerable<CategoryGrade>> GetAllAsync();
        Task<CategoryGrade> GetByIdAsync(Guid id);
        Task AddAsync(CategoryGrade categoryGrade);
        Task UpdateAsync(CategoryGrade categoryGrade);
        Task DeleteAsync(Guid id);

    }
}
