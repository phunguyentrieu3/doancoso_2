﻿using DoAnCoSoPhanModel_DB.DataAccess;
using DoAnCoSoPhanModel_DB.Models;
using Microsoft.EntityFrameworkCore;

namespace DoAnCoSoPhanModel_DB.Repositories
{
    public class EFPostRepository : IPostRepository
    {
        private readonly ApplicationDbContext _context;
        public EFPostRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task AddAsync(Post post)
        {
            _context.posts.Add(post);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            var post1 = await _context.posts.FindAsync(id);
            _context.posts.Remove(post1);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Post>> GetAllAsync()
        {
            return await _context.posts.ToListAsync();
        }

        public async Task<Post> GetByIdAsync(Guid id)
        {
            return await _context.posts.FindAsync(id);
        }

        public async Task UpdateAsync(Post post)
        {
            _context.posts.Update(post);
            await _context.SaveChangesAsync();
        }
    }
}
