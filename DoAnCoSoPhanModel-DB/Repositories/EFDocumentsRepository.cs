﻿using DoAnCoSoPhanModel_DB.DataAccess;
using DoAnCoSoPhanModel_DB.Models;
using Microsoft.EntityFrameworkCore;

namespace DoAnCoSoPhanModel_DB.Repositories
{
    public class EFDocumentsRepository : IDocumentsRepository
    {
        private readonly ApplicationDbContext _context;
        public EFDocumentsRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task AddAsync(Documents documents)
        {
            _context.documents.Add(documents);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            var documents1 = await _context.documents.FindAsync(id);
            _context.documents.Remove(documents1);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Documents>> GetAllAsync()
        {
            return await _context.documents.ToListAsync();
        }

        public async Task<Documents> GetByIdAsync(Guid id)
        {
            return await _context.documents.FindAsync(id);
        }

        public async Task UpdateAsync(Documents documents)
        {
            _context.documents.Update(documents);
            await _context.SaveChangesAsync();
        }
        public List<Documents> GetAll()
        {
            return _context.documents.ToList();
        }
    }
}
