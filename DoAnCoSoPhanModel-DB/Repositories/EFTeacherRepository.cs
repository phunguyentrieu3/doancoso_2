﻿using DoAnCoSoPhanModel_DB.DataAccess;
using DoAnCoSoPhanModel_DB.Models;
using Microsoft.EntityFrameworkCore;

namespace DoAnCoSoPhanModel_DB.Repositories
{
    public class EFTeacherRepository : ITeacherRepository
    {
        private readonly ApplicationDbContext _context;
        public EFTeacherRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task AddAsync(Teacher teacher)
        {
            _context.teachers_detail.Add(teacher);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            var teacher1 = await _context.teachers_detail.FindAsync(id);
            _context.teachers_detail.Remove(teacher1);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Teacher>> GetAllAsync()
        {
            return await _context.teachers_detail.ToListAsync();
        }

        public async Task<Teacher> GetByIdAsync(Guid id)
        {
            return await _context.teachers_detail.FindAsync(id);
        }

        public async Task UpdateAsync(Teacher teacher)
        {
            _context.teachers_detail.Update(teacher);
            await _context.SaveChangesAsync();
        }
        public List<Teacher> GetAll()
        {
            return _context.teachers_detail.ToList();
        }
    }
}
