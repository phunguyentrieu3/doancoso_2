﻿using DoAnCoSoPhanModel_DB.Models;
using DoAnCoSoPhanModel_DB.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DoAnCoSoPhanModel_DB.Controllers
{
    public class CommentsController : Controller
    {
        private readonly ICommentsRepository _commentsRepository;

        public CommentsController(ICommentsRepository commentsRepository)
        {
            _commentsRepository = commentsRepository;
        }
        // GET: CommentsController
        public async Task<ActionResult> IndexCmt()
        {
            var comment = await _commentsRepository.GetAllAsync();
            return View(comment);
        }

        // GET: CommentsController/Details/5
        public async Task<ActionResult> DetailsCmt(Guid id)
        {
            var comment = await _commentsRepository.GetByIdAsync(id);
            return View(comment);
        }

        // GET: CommentsController/Create
        public async Task<ActionResult> CreateCmt()
        {
            var comment = await _commentsRepository.GetAllAsync();
            return View();
        }

        // POST: CommentsController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateCmt(Comments comments)
        {
            if (ModelState.IsValid)
            {
                comments.time_create = DateTime.Now;
                await _commentsRepository.AddAsync(comments);
                return RedirectToAction(nameof(IndexCmt));

            }
            return View(comments);
        }


        // GET: CommentsController/Edit/5
        public async Task<ActionResult> EditCmt(Guid id)
        {
            var comment = await _commentsRepository.GetByIdAsync(id);
            if (comment == null)
            {
                return NotFound();
            }

            return View(comment);
        }

        // POST: CommentsController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditCmt(Guid id, Comments editComments)
        {
            try
            {
                if (id != editComments.Id)
                {
                    return NotFound();
                }

                if (ModelState.IsValid)
                {
                    var existingComment = await _commentsRepository.GetByIdAsync(id);
                    if (existingComment == null)
                    {
                        return NotFound();
                    }

                    // Update other properties
                    existingComment.title = editComments.title;
                    existingComment.contents = editComments.contents;
                    existingComment.time_update = DateTime.Now; // Update the time of update

                    await _commentsRepository.UpdateAsync(existingComment);

                    return RedirectToAction(nameof(IndexCmt));
                }
                return RedirectToAction(nameof(IndexCmt));
            }
            catch
            {
                return View(editComments);
            }
        }

        // GET: CommentsController/Delete/5
        public async Task<ActionResult> DeleteCmt(Guid id)
        {
            var comment = await _commentsRepository.GetByIdAsync(id);
            if (comment == null)
            {
                return NotFound();
            }
            return View(comment);
        }

        // POST: CommentsController/Delete/5
        [HttpPost, ActionName("DeleteCmt")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteCmt(Guid id, IFormCollection collection)
        {
            try
            {
                await _commentsRepository.DeleteAsync(id);
                return RedirectToAction(nameof(IndexCmt));
            }
            catch
            {
                return View();
            }
        }
    }
}
