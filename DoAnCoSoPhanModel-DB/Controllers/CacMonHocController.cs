﻿using DoAnCoSoPhanModel_DB.Models;
using DoAnCoSoPhanModel_DB.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace DoAnCoSoPhanModel_DB.Controllers
{
    public class CacMonHocController : Controller
    {
        private readonly ISubjectRepository _subjectRepository;
        private readonly IDocumentsRepository _documentsRepository;

        public CacMonHocController(ISubjectRepository subjectRepository, IDocumentsRepository documentsRepository)
        {
            _subjectRepository = subjectRepository;
            _documentsRepository = documentsRepository;
        }

        public async Task<IActionResult> NguoiDungSubject()
        {
            var gv = await _subjectRepository.GetAllAsync();

            return View(gv);
        }
        public async Task<IActionResult> DetailsSubject(Guid id)
        {
            var subject = await _subjectRepository.GetByIdAsync(id);
            if (subject == null)
            {
                return NotFound();
            }
            return View(subject);
        }

    }
}
