﻿using DoAnCoSoPhanModel_DB.Models;
using DoAnCoSoPhanModel_DB.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.IO;
using System.Threading.Tasks;

namespace DoAnCoSoPhanModel_DB.Controllers
{
    public class PostController : Controller
    {
        private readonly IPostRepository _postRepository;

        public PostController(IPostRepository postRepository)
        {
            _postRepository = postRepository;
        }

        public async Task<IActionResult> IndexList()
        {
            var posts = await _postRepository.GetAllAsync();
            return View(posts);
        }

        public async Task<IActionResult> IndexPost()
        {
            var indexPost = await _postRepository.GetAllAsync();

            return View(indexPost);
        }


        public async Task<IActionResult> AddPost()
        {
            var post = await _postRepository.GetAllAsync();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddPost(Post post, IFormFile ImageUrl)
        {
            if (ModelState.IsValid)
            {
                if (ImageUrl != null)
                {
                    // Lưu hình ảnh đại diện
                    post.ImageUrl = await SaveImage(ImageUrl);
                }
                post.time_create = DateTime.Now;
                await _postRepository.AddAsync(post);
                return RedirectToAction(nameof(IndexPost));
            }
            return View(post);
        }

        private async Task<string> SaveImage(IFormFile ImageUrl)
        {
            var savePath = Path.Combine("wwwroot/Post/images", ImageUrl.FileName); // Thay đổi đường dẫn theo cấu hình của bạn
            using (var fileStream = new FileStream(savePath, FileMode.Create))
            {
                await ImageUrl.CopyToAsync(fileStream);
            }
            return "/Post/images/" + ImageUrl.FileName; // Trả về đường dẫn tương đối
        }

        public async Task<IActionResult> DisplayPost(Guid id)
        {
            var post = await _postRepository.GetByIdAsync(id);
            if (post == null)
            {
                return NotFound();
            }
            return View(post);
        }

        public async Task<IActionResult> DeletePost(Guid id)
        {
            var post = await _postRepository.GetByIdAsync(id);
            if (post == null)
            {
                return NotFound();
            }
            return View(post);
        }

        [HttpPost, ActionName("DeleteConfirmed")]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            await _postRepository.DeleteAsync(id);
            return RedirectToAction(nameof(IndexPost));
        }

        public async Task<IActionResult> UpdatePost(Guid id)
        {
            var post = await _postRepository.GetByIdAsync(id);
            if (post == null)
            {
                return NotFound();
            }

            return View(post);
        }

        [HttpPost]
        public async Task<IActionResult> UpdatePost(Guid id, Post updatedPost, IFormFile ImageUrl)
        {
            ModelState.Remove("ImageUrl");
            var existingPost = await _postRepository.GetByIdAsync(id);
            if (id != updatedPost.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                if (ImageUrl != null)
                {
                    // Lưu hình ảnh đại diện
                    updatedPost.ImageUrl = await SaveImage(ImageUrl);
                }
                else
                {
                    updatedPost.ImageUrl = existingPost.ImageUrl;
                }

                // Update other properties

                existingPost.title = updatedPost.title;
                existingPost.Content = updatedPost.Content;

                existingPost.time_update = DateTime.Now;


                existingPost.ImageUrl = updatedPost.ImageUrl;
                existingPost.time_create = existingPost.time_create;

                await _postRepository.UpdateAsync(existingPost);

                return RedirectToAction(nameof(IndexPost));
            }

            return View(updatedPost);
        }


    }
}
