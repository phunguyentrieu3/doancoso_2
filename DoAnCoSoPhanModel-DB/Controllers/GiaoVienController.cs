﻿using DoAnCoSoPhanModel_DB.Models;
using DoAnCoSoPhanModel_DB.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace DoAnCoSoPhanModel_DB.Controllers
{
    public class GiaoVienController : Controller
    {
        private readonly ITeacherRepository _teacherRepository;

        public GiaoVienController(ITeacherRepository teacherRepository)
        {
            _teacherRepository = teacherRepository;
        }
        public async Task<IActionResult> IndexGiaoVien()
        {
            var gv = await _teacherRepository.GetAllAsync();
            return View(gv);
        }


        private async Task<string> SaveImage(IFormFile ImageUrl)
        {
            var savePath = Path.Combine("wwwroot/Teacher/images", ImageUrl.FileName); // Thay đổi đường dẫn theo cấu hình của bạn
            using (var fileStream = new FileStream(savePath, FileMode.Create))
            {
                await ImageUrl.CopyToAsync(fileStream);
            }
            return "/Teacher/images/" + ImageUrl.FileName; // Trả về đường dẫn tương đối
        }

        public async Task<IActionResult> DetailsGiaoVien(Guid id)
        {
            var gv = await _teacherRepository.GetByIdAsync(id);
            if (gv == null)
            {
                return NotFound();
            }
            return View(gv);
        }
        public async Task<IActionResult> GetAllGiaoVien()
        {
            var gv = await _teacherRepository.GetAllAsync();
            ViewBag.NumberOfFB = gv.Count();
            return View(gv);
        }

    }
}
