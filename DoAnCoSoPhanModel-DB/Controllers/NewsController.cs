﻿using DoAnCoSoPhanModel_DB.Models;
using DoAnCoSoPhanModel_DB.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace DoAnCoSoPhanModel_DB.Controllers
{
    public class NewsController : Controller
    {
        private readonly INewsRepository _newsRepository;

        public NewsController(INewsRepository newsRepository)
        {
            _newsRepository = newsRepository;
        }
        public async Task<IActionResult> IndexNews()
        {
            var news = await _newsRepository.GetAllAsync();
            ViewBag.NumberOfFB = news.Count();
            return View(news);
        }

        public async Task<IActionResult> NguoiDungNews()
        {
            var gv = await _newsRepository.GetAllAsync();
            
            return View(gv);
        }


        public async Task<IActionResult> CreateNews()
        {
            var news = await _newsRepository.GetAllAsync();
            return View();
        }


        [HttpPost]
        public async Task<IActionResult> CreateNews(News news, IFormFile ImageUrl)
        {
            if (ModelState.IsValid)
            {
                if (ImageUrl != null)
                {
                    // Lưu hình ảnh đại diện
                    news.ImageUrl = await SaveImage(ImageUrl);
                }
                news.time_create = DateTime.Now;
                await _newsRepository.AddAsync(news);
                return RedirectToAction(nameof(NguoiDungNews));
            }

            return View(news);
        }

        private async Task<string> SaveImage(IFormFile ImageUrl)
        {
            var savePath = Path.Combine("wwwroot/New/images", ImageUrl.FileName); // Thay đổi đường dẫn theo cấu hình của bạn
            using (var fileStream = new FileStream(savePath, FileMode.Create))
            {
                await ImageUrl.CopyToAsync(fileStream);
            }
            return "/New/images/" + ImageUrl.FileName; // Trả về đường dẫn tương đối
        }

        public async Task<IActionResult> DetailsNews(Guid id)
        {
            var news = await _newsRepository.GetByIdAsync(id);
            if (news == null)
            {
                return NotFound();
            }
            return View(news);
        }


        public async Task<IActionResult> EditNews(Guid id)
        {
            var news = await _newsRepository.GetByIdAsync(id);
            if (news == null)
            {
                return NotFound();
            }
            return View(news);
        }
        // Process the product update
        [HttpPost]
        public async Task<IActionResult> Edit(Guid id, News news, IFormFile ImageUrl)
        {
            ModelState.Remove("ImageUrl");
            var existingNews = await _newsRepository.GetByIdAsync(id);
            if (id != news.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                if (ImageUrl != null)
                {
                    // Lưu hình ảnh đại diện
                    news.ImageUrl = await SaveImage(ImageUrl);
                }
                else
                {
                    news.ImageUrl = existingNews.ImageUrl;
                }
                existingNews.Title = news.Title;
                existingNews.Content = news.Content;

                existingNews.time_update = DateTime.Now;

                existingNews.ImageUrl = news.ImageUrl;
                existingNews.time_create = existingNews.time_create;
                await _newsRepository.UpdateAsync(existingNews);
                return RedirectToAction(nameof(IndexNews));
            }

            return View(news);
        }

        public async Task<IActionResult> DeleteNews(Guid id)
        {
            var news = await _newsRepository.GetByIdAsync(id);
            if (news == null)
            {
                return NotFound();
            }
            return View(news);
        }
        // Xử lý xóa sản phẩm
        [HttpPost, ActionName("DeleteNews")]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            await _newsRepository.DeleteAsync(id);
            return RedirectToAction(nameof(IndexNews));
        }
    }
}
