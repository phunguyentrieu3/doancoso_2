﻿
using DoAnCoSoPhanModel_DB.Models;
using DoAnCoSoPhanModel_DB.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace DoAnCoSoPhanModel_DB.Controllers
{
    public class StudentController : Controller
    {
        private readonly IStudentRepository _studentRepository;

        public StudentController(IStudentRepository studentRepository)
        {
            _studentRepository = studentRepository;
        }
        public async Task<IActionResult> IndexHocSinh()
        {
            var hs = await _studentRepository.GetAllAsync();
            return View(hs);
        }


        public async Task<IActionResult> CreateHocSinh()
        {
            var hs = await _studentRepository.GetAllAsync();
            return View();
        }


        [HttpPost]
        public async Task<IActionResult> CreateHocSinh(Student hs, IFormFile ImageUrl)
        {
            if (ModelState.IsValid)
            {
                if (ImageUrl != null)
                {
                    // Lưu hình ảnh đại diện
                    hs.ImageUrl = await SaveImage(ImageUrl);
                }
                await _studentRepository.AddAsync(hs);
                return RedirectToAction(nameof(IndexHocSinh));
            }

            return View(hs);
        }

        private async Task<string> SaveImage(IFormFile ImageUrl)
        {
            var savePath = Path.Combine("wwwroot/Student/images", ImageUrl.FileName); // Thay đổi đường dẫn theo cấu hình của bạn
            using (var fileStream = new FileStream(savePath, FileMode.Create))
            {
                await ImageUrl.CopyToAsync(fileStream);
            }
            return "/Student/images/" + ImageUrl.FileName; // Trả về đường dẫn tương đối
        }

        //===============================================
        public async Task<IActionResult> EditHocSinh(Guid id)
        {
            var hs = await _studentRepository.GetByIdAsync(id);
            if (hs == null)
            {
                return NotFound();
            }
            return View(hs);
        }

        [HttpPost]
        public async Task<IActionResult> EditHocSinh(Guid id, Student hs, IFormFile ImageUrl)
        {
            ModelState.Remove("ImageUrl");
            var existingHS = await _studentRepository.GetByIdAsync(id);
            if (id != hs.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                if (ImageUrl != null)
                {
                    // Lưu hình ảnh đại diện mới nếu có
                    hs.ImageUrl = await SaveImage(ImageUrl);
                }
                else
                {
                    // Giữ nguyên hình ảnh đại diện cũ nếu không có hình ảnh mới
                    hs.ImageUrl = existingHS.ImageUrl;
                }

                // Cập nhật thông tin của giáo viên từ form vào giáo viên hiện tại
                existingHS.Email = hs.Email;
                existingHS.FullName = hs.FullName;
                existingHS.Grade = hs.Grade;
                existingHS.Gender = hs.Gender;
                existingHS.Address = hs.Address;
                existingHS.PhoneNumber = hs.PhoneNumber;
                existingHS.ImageUrl = hs.ImageUrl; // Gán hình ảnh mới hoặc giữ nguyên hình ảnh cũ

                // Lưu thông tin giáo viên đã được cập nhật
                await _studentRepository.UpdateAsync(existingHS);

                return RedirectToAction(nameof(IndexHocSinh));
            }

            return View(hs);
        }

        public async Task<IActionResult> DetailsHocSinh(Guid id)
        {
            var hs = await _studentRepository.GetByIdAsync(id);
            if (hs == null)
            {
                return NotFound();
            }
            return View(hs);
        }


        public async Task<IActionResult> DeleteHocSinh(Guid id)
        {
            var hs = await _studentRepository.GetByIdAsync(id);
            if (hs == null)
            {
                return NotFound();
            }
            return View(hs);
        }
        // Xử lý xóa sản phẩm
        [HttpPost, ActionName("DeleteHocSinh")]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            await _studentRepository.DeleteAsync(id);
            return RedirectToAction(nameof(IndexHocSinh));
        }


    }
}
