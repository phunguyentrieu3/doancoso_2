﻿using DoAnCoSoPhanModel_DB.Models;
using DoAnCoSoPhanModel_DB.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace DoAnCoSoPhanModel_DB.Controllers
{
    public class FeedbackController : Controller
    {
        private readonly IFeedBacksRepository _feedBacksRepository;

        public FeedbackController(IFeedBacksRepository feedBacksRepository)
        {
            _feedBacksRepository = feedBacksRepository;
        }
        public async Task<IActionResult> IndexFeedBack()
        {
            var feedback = await _feedBacksRepository.GetAllAsync();
            return View(feedback);
        }

        public async Task<IActionResult> DetailsFeedback(Guid id)
        {
            var feedbacks = await _feedBacksRepository.GetByIdAsync(id);
            if (feedbacks == null)
            {
                return NotFound();
            }
            return View(feedbacks);
        }

        public async Task<IActionResult> CreateFeedback()
        {
            var feedbacks = await _feedBacksRepository.GetAllAsync();

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateFeedback(Feedbacks feedbacks)
        {
            if (ModelState.IsValid)
            {
                await _feedBacksRepository.AddAsync(feedbacks);
                return RedirectToAction(nameof(IndexFeedBack));
            }
            // Nếu ModelState không hợp lệ, hiển thị form với dữ liệu đã nhập

            return View(feedbacks);
        }

        public async Task<IActionResult> EditFeedback(Guid id)
        {
            var feedbacks = await _feedBacksRepository.GetByIdAsync(id);
            if (feedbacks == null)
            {
                return NotFound();
            }
            return View(feedbacks);
        }

        [HttpPost]
        public async Task<IActionResult> EditFeedback(Guid id, Feedbacks fb)
        {
            var existingFb = await _feedBacksRepository.GetByIdAsync(id);
            if (id != fb.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                existingFb.title = fb.title;
                existingFb.content = fb.content;

                await _feedBacksRepository.UpdateAsync(existingFb);
                return RedirectToAction(nameof(IndexFeedBack));
            }

            return View(fb);
        }


    }
}
