﻿using DoAnCoSoPhanModel_DB.Models;
using DoAnCoSoPhanModel_DB.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace DoAnCoSoPhanModel_DB.Controllers
{
    public class DocumentController : Controller
    {
        private readonly IDocumentsRepository _documentsRepository;

        public DocumentController(IDocumentsRepository documentsRepository)
        {
            _documentsRepository = documentsRepository;
        }

        public async Task<IActionResult> NguoiDungDocument()
        {
            var d = await _documentsRepository.GetAllAsync();

            return View(d);
        }

        public async Task<IActionResult> DetailsDocument(Guid id)
        {
            var d = await _documentsRepository.GetByIdAsync(id);
            if (d == null)
            {
                return NotFound();
            }
            return View(d);
        }
    }
}
